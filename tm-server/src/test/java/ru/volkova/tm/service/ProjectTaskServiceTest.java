package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.dto.IProjectTaskDTOService;
import ru.volkova.tm.api.service.dto.ITaskDTOService;
import ru.volkova.tm.dto.ProjectDTO;
import ru.volkova.tm.dto.TaskDTO;
import ru.volkova.tm.dto.UserDTO;
import ru.volkova.tm.marker.UnitCategory;
import ru.volkova.tm.service.dto.ProjectTaskDTOService;
import ru.volkova.tm.service.dto.TaskDTOService;
import ru.volkova.tm.service.model.ProjectTaskService;
import ru.volkova.tm.service.model.TaskService;

import java.util.List;

public class ProjectTaskServiceTest {

    private final IPropertyService propertyService = new PropertyService();

    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private final IProjectTaskDTOService projectTaskService = new ProjectTaskDTOService(connectionService);

    @NotNull
    private final UserDTO user = new UserDTO();

    @Test
    @Category(UnitCategory.class)
    public void bindTaskByProjectIdTest() {
        final ProjectDTO project = new ProjectDTO();
        final TaskDTO task = new TaskDTO();
        task.setUser(user);
        taskService.insert(task);
        projectTaskService.bindTaskByProjectId(task.getUser().getId(), project.getId(), task.getId());
        Assert.assertNotNull(task.getProject().getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllByProjectIdTest() {
        final ProjectDTO project = new ProjectDTO();
        project.setUser(user);
        final TaskDTO task1 = new TaskDTO();
        final TaskDTO task2 = new TaskDTO();
        task1.setUser(user);
        task2.setUser(user);
        taskService.insert(task1);
        taskService.insert(task2);
        projectTaskService.bindTaskByProjectId(user.getId(), project.getId(), task1.getId());
        projectTaskService.bindTaskByProjectId(user.getId(), project.getId(), task2.getId());
        List<TaskDTO> taskList = projectTaskService.findAllTasksByProjectId(project.getUser().getId(), project.getId());
        Assert.assertNotNull(taskList.contains(task1));
        Assert.assertTrue(taskList.contains(task2));
    }

    @Test
    @Category(UnitCategory.class)
    public void unbindTaskByProjectIdTest() {
        final ProjectDTO project = new ProjectDTO();
        final TaskDTO task = new TaskDTO();
        task.setUser(user);
        taskService.insert(task);
        projectTaskService.bindTaskByProjectId(task.getUser().getId(), project.getId(), task.getId());
        projectTaskService.unbindTaskByProjectId(task.getUser().getId(), project.getId(), task.getId());
        Assert.assertNull(task.getProject().getId());
    }

}
