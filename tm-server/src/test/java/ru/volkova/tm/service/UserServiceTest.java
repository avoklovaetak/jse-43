package ru.volkova.tm.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.api.service.dto.IAdminUserDTOService;
import ru.volkova.tm.api.service.dto.IUserDTOService;
import ru.volkova.tm.api.service.model.IAdminUserService;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.model.IUserService;
import ru.volkova.tm.dto.UserDTO;
import ru.volkova.tm.marker.UnitCategory;
import ru.volkova.tm.service.dto.AdminUserDTOService;
import ru.volkova.tm.service.dto.UserDTOService;
import ru.volkova.tm.service.model.AdminUserService;
import ru.volkova.tm.service.model.UserService;

public class UserServiceTest {

    private final IPropertyService propertyService = new PropertyService();

    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private final IAdminUserDTOService adminUserService = new AdminUserDTOService(connectionService, propertyService);

    private final IUserDTOService userService = new UserDTOService(connectionService);

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final UserDTO user = new UserDTO();
        Assert.assertNotNull(adminUserService.add(user));
    }

}
