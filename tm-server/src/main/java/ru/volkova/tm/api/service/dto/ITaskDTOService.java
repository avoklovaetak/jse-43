package ru.volkova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.TaskDTO;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.model.Task;

import java.util.List;

public interface ITaskDTOService extends IServiceDTO<TaskDTO> {

    TaskDTO insert(@NotNull TaskDTO task);

    void add(
            @NotNull String userId,
            @Nullable String name,
            @Nullable String description
    );

    void addAll(@Nullable List<TaskDTO> entities);

    void clear(@NotNull String userId);

    List<TaskDTO> findAll(@NotNull String userId);

    @Nullable
    TaskDTO findById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    TaskDTO findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    TaskDTO findOneByName(@NotNull String userId, @NotNull String name);

    void changeOneStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @Nullable final Status status
    );

    void changeOneStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @Nullable final Status status
    );

    void removeById(@NotNull String userId,@NotNull String id);

    void removeOneByName(@NotNull String userId,@Nullable String name);

    void updateOneById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
