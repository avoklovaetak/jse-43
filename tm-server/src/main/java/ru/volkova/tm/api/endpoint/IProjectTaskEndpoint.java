package ru.volkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.SessionDTO;
import ru.volkova.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectTaskEndpoint {

    @WebMethod
    void bindTaskByProjectId(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "projectId", partName = "projectId") String projectId,
            @NotNull @WebParam(name = "taskId", partName = "taskId") String taskId
    );

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTasksByProjectId(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "projectId", partName = "projectId") String projectId
    );

    @WebMethod
    void removeProjectById(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    void unbindTaskByProjectId(
            @NotNull @WebParam(name = "session", partName = "session") SessionDTO session,
            @Nullable @WebParam(name = "projectId", partName = "projectId") String projectId,
            @NotNull @WebParam(name = "taskId", partName = "taskId") String taskId
    );

}
