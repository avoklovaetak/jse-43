package ru.volkova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IRepositoryDTO;
import ru.volkova.tm.dto.ProjectDTO;
import ru.volkova.tm.enumerated.Status;

import java.util.List;

public interface IProjectDTORepository extends IRepositoryDTO<ProjectDTO> {

    @Nullable
    ProjectDTO insert(@Nullable final ProjectDTO project);

    void clear(@NotNull String userId);

    @NotNull
    List<ProjectDTO> findAll(@NotNull String userId);

    @Nullable
    ProjectDTO findById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    ProjectDTO findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    ProjectDTO findOneByName(@NotNull String userId, @NotNull String name);

    void changeOneStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    );

    void changeOneStatusByName(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final Status status
    );

    void removeById(@NotNull String userId,@NotNull String id);

    void removeOneByName(@NotNull String userId,@Nullable String name);

    void updateOneById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
