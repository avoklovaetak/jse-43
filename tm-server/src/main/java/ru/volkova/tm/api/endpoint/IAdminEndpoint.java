package ru.volkova.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminEndpoint {

    @WebMethod
    void dataBinarySave(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void dataBinaryLoad(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void dataJsonLoad(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void dataJsonSave(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void dataBase64Load(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void dataBase64Save(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void dataJsonLoadFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void dataJsonSaveFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void dataJsonLoadJaxb(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void dataJsonSaveJaxb(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void dataXmlLoadFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void dataXmlSaveFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void dataXmlLoadJaxb(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void dataXmlSaveJaxb(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void dataYamlLoadFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void dataYamlSaveFasterxml(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO session
    );

}
