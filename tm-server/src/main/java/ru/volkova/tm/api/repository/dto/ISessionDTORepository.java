package ru.volkova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.repository.IRepositoryDTO;
import ru.volkova.tm.dto.SessionDTO;

public interface ISessionDTORepository extends IRepositoryDTO<SessionDTO> {

    void add(@NotNull SessionDTO session);

    void close(@NotNull SessionDTO session);

}
