package ru.volkova.tm.api.service.model;

import ru.volkova.tm.api.repository.IRepository;
import ru.volkova.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
