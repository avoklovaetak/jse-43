package ru.volkova.tm.api.service.dto;

import ru.volkova.tm.api.repository.IRepositoryDTO;
import ru.volkova.tm.dto.AbstractEntityDTO;

public interface IServiceDTO<E extends AbstractEntityDTO> extends IRepositoryDTO<E> {

}
