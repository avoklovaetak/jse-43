package ru.volkova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.repository.IRepository;
import ru.volkova.tm.model.Session;

public interface ISessionRepository extends IRepository<Session> {

    void add(@NotNull Session session);

    void close(@NotNull Session session);

}
