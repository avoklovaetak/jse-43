package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.service.dto.*;
import ru.volkova.tm.api.service.model.*;

public interface ServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    IBackupService getBackupService();

    @NotNull
    IAdminUserService getAdminUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IAdminService getAdminService();

    @NotNull
    IConnectionService getConnectionService();

    @NotNull
    IAdminUserDTOService getAdminUserDTOService();

    @NotNull
    IProjectDTOService getProjectDTOService();

    @NotNull
    IProjectTaskDTOService getProjectTaskDTOService();

    @NotNull
    ISessionDTOService getSessionDTOService();

    @NotNull
    ITaskDTOService getTaskDTOService();

    @NotNull
    IUserDTOService getUserDTOService();

}
