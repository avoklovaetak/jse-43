package ru.volkova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IRepositoryDTO;
import ru.volkova.tm.dto.UserDTO;
import ru.volkova.tm.model.User;

import java.util.List;

public interface IUserDTORepository extends IRepositoryDTO<UserDTO> {

    @NotNull
    UserDTO insert(@Nullable UserDTO user);

    void clear();

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    @Nullable
    UserDTO findById(@NotNull String id);

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    void lockByEmail(@NotNull String email);

    void lockById(@NotNull String id);

    void lockByLogin(@NotNull String login);

    void removeByEmail(@NotNull String email);

    void removeById(@NotNull String id);

    void removeByLogin(@NotNull String login);

    void unlockByEmail(@NotNull String email);

    void unlockById(@NotNull String id);

    void setPassword(
            @NotNull final String userId,
            @Nullable final String hash
    );

    void unlockByLogin(@NotNull String login);

    void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String secondName,
            @Nullable final String middleName
    );

}
