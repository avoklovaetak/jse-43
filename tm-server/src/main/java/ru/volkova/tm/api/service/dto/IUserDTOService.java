package ru.volkova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.UserDTO;

public interface IUserDTOService extends IServiceDTO<UserDTO> {

    void setPassword(@NotNull String userId, @Nullable String password);

}
