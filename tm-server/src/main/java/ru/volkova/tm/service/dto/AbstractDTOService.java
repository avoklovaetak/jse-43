package ru.volkova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.dto.IServiceDTO;
import ru.volkova.tm.dto.AbstractEntityDTO;

public abstract class AbstractDTOService<E extends AbstractEntityDTO> implements IServiceDTO<E> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractDTOService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
