package ru.volkova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.api.service.model.IAdminUserService;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.auth.EmailExistsException;
import ru.volkova.tm.exception.auth.LoginExistsException;
import ru.volkova.tm.exception.empty.*;
import ru.volkova.tm.exception.entity.UserNotFoundException;
import ru.volkova.tm.model.User;
import ru.volkova.tm.repository.model.UserRepository;
import ru.volkova.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class AdminUserService extends AbstractService<User> implements IAdminUserService {

    private final IPropertyService propertyService;

    public AdminUserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService
            )
    {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            userRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<User> entities) {
        if (entities == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            entities.forEach(userRepository::insert);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public User add(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            userRepository.insert(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User createUser(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        add(user);
        return user;
    }

    @NotNull
    @Override
    public User createUserWithEmail(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        User user = new User();
        user.setEmail(email);
        final String passwordHash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(passwordHash);
        user.setLogin(login);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public User createUserWithRole
            (@Nullable final String login,
             @Nullable final String password,
             @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        User user = new User();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setLogin(login);
        user.setRole(role);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            final List<User> users = userRepository.findAll();
            entityManager.getTransaction().commit();
            return users;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            final User user = userRepository.findByEmail(email);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            final User user = userRepository.findById(id);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            final User user = userRepository.findByLogin(login);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        @Nullable final User user = findByEmail(email);
        return user != null;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        @Nullable final User user = findByLogin(login);
        return user != null;
    }

    @Override
    public void lockByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            userRepository.lockByEmail(email);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void lockById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            userRepository.lockById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void lockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            userRepository.lockByLogin(login);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            userRepository.removeByEmail(email);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            userRepository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            userRepository.removeByLogin(login);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


    @Override
    public void unlockByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            userRepository.unlockByEmail(email);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            userRepository.unlockById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            userRepository.unlockByLogin(login);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String secondName,
            @Nullable final String middleName
    ) {
        @Nullable final User user = findById(userId);
        if (user == null) throw new UserNotFoundException();
        if (firstName == null || firstName.isEmpty()
                || secondName == null || secondName.isEmpty()
                || middleName == null || middleName.isEmpty())
            throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            userRepository.updateUser(userId, firstName, secondName, middleName);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}

