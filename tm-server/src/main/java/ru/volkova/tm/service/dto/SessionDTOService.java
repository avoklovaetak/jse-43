package ru.volkova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.api.service.dto.ISessionDTOService;
import ru.volkova.tm.dto.SessionDTO;
import ru.volkova.tm.dto.UserDTO;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.model.User;
import ru.volkova.tm.repository.dto.SessionDTORepository;
import ru.volkova.tm.repository.dto.UserDTORepository;
import ru.volkova.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionDTOService extends AbstractDTOService<SessionDTO> implements ISessionDTOService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionDTOService(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final IConnectionService connectionService) {
        super(connectionService);
        this.serviceLocator = serviceLocator;
    }

    public void add(@NotNull SessionDTO session) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final SessionDTORepository sessionRepository = new SessionDTORepository(entityManager) ;
            sessionRepository.add(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserDTORepository userRepository = new UserDTORepository(entityManager);
            @Nullable final UserDTO user = userRepository.findByLogin(login);
            if (user == null) return null;
            @NotNull final SessionDTO session = new SessionDTO();
            session.setUser(user);
            @Nullable final SessionDTO signSession = sign(session);
            if (signSession == null) return null;
            @NotNull final SessionDTORepository sessionRepository = new SessionDTORepository(entityManager) ;
            sessionRepository.add(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final User user = serviceLocator.getAdminUserService().findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        final String passwordHash2 = user.getPasswordHash();
        return passwordHash.equals(passwordHash2);
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        final String signature = session.getSignature();
        if (signature == null || signature.isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionDTO temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final SessionDTO sessionTarget = sign(temp);
        if (sessionTarget == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validateAdmin(@Nullable final SessionDTO session, @Nullable final Role role) {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        validate(session);
        final @Nullable User user = serviceLocator.getAdminUserService().findById(session.getUser().getId());
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() != Role.ADMIN) throw new AccessDeniedException();
    }

    @Override
    @Nullable
    public SessionDTO close(@Nullable final SessionDTO session) {
        if (session == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final SessionDTORepository sessionRepository = new SessionDTORepository(entityManager) ;
            sessionRepository.close(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public List<SessionDTO> findAll() {
        return null;
    }

    @Nullable
    public SessionDTO sign(@Nullable final SessionDTO session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @Nullable final String signature = HashUtil.salt(propertyService, session);
        session.setSignature(signature);
        return session;
    }

}
