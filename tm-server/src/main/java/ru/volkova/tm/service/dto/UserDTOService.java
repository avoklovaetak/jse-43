package ru.volkova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.dto.IUserDTOService;
import ru.volkova.tm.dto.UserDTO;
import ru.volkova.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;

public final class UserDTOService extends AbstractDTOService<UserDTO> implements IUserDTOService {

    public UserDTOService(
            @NotNull final IConnectionService connectionService
    ) {
        super(connectionService);
    }

    public void setPassword(
            @NotNull final String userId,
            @Nullable final String password
    ) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager) ;
            userRepository.setPassword(userId, password);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
