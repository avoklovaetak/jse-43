package ru.volkova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.service.model.IService;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.model.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
