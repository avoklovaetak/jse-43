package ru.volkova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.dto.IProjectDTOService;
import ru.volkova.tm.dto.ProjectDTO;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.repository.dto.ProjectDTORepository;
import ru.volkova.tm.repository.dto.UserDTORepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectDTOService extends AbstractDTOService<ProjectDTO> implements IProjectDTOService {

    public ProjectDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @SneakyThrows
    @Override
    public void insert(@Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTORepository projectRepository = new ProjectDTORepository(entityManager) ;
            projectRepository.insert(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void add(
            @NotNull String userId,
            @Nullable String name,
            @Nullable String description
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new ObjectNotFoundException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final UserDTORepository userRepository = new UserDTORepository(entityManager);
            project.setUser(userRepository.findById(userId));
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTORepository projectRepository = new ProjectDTORepository(entityManager) ;
            projectRepository.insert(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<ProjectDTO> entities) {
        if (entities == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTORepository projectRepository = new ProjectDTORepository(entityManager) ;
            entities.forEach(projectRepository::insert);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTORepository projectRepository = new ProjectDTORepository(entityManager) ;
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@NotNull String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTORepository projectRepository = new ProjectDTORepository(entityManager) ;
            final List<ProjectDTO> projects = projectRepository.findAll(userId);
            entityManager.getTransaction().commit();
            return projects;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO findById(@NotNull String userId, @NotNull String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTORepository projectRepository = new ProjectDTORepository(entityManager) ;
            final ProjectDTO project = projectRepository.findById(userId,id);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIndex(@NotNull String userId, @NotNull Integer index) {
        if (index < 0) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTORepository projectRepository = new ProjectDTORepository(entityManager) ;
            final ProjectDTO project = projectRepository.findOneByIndex(userId,index);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO findOneByName(@NotNull String userId, @NotNull String name) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTORepository projectRepository = new ProjectDTORepository(entityManager) ;
            final ProjectDTO project = projectRepository.findOneByName(userId,name);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeOneStatusById(@NotNull String userId, @NotNull String id, @Nullable Status status) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTORepository projectRepository = new ProjectDTORepository(entityManager) ;
            projectRepository.changeOneStatusById(userId, id, status);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeOneStatusByName(@NotNull String userId, @NotNull String name, @Nullable Status status) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTORepository projectRepository = new ProjectDTORepository(entityManager) ;
            projectRepository.changeOneStatusByName(userId, name, status);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTORepository projectRepository = new ProjectDTORepository(entityManager) ;
            projectRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneByName(@NotNull String userId, @NotNull String name) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTORepository projectRepository = new ProjectDTORepository(entityManager) ;
            projectRepository.removeOneByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateOneById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTORepository projectRepository = new ProjectDTORepository(entityManager) ;
            projectRepository.updateOneById(userId, id, name, description);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
