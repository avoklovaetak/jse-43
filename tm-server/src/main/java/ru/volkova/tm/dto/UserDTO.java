package ru.volkova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "user")
public class UserDTO extends AbstractEntityDTO {

    @Column
    @Nullable
    private String login;

    @Column(name = "password_hash")
    @Nullable
    private String passwordHash;

    @Column
    @Nullable
    private String email;

    @Column(name = "middle_name")
    @Nullable
    private String middleName;

    @Column(name = "first_name")
    @Nullable
    private String firstName;

    @Column(name = "last_name")
    @Nullable
    private String secondName;

    @Column
    @NotNull
    private Boolean locked = true;

    @Enumerated(EnumType.STRING)
    @Nullable
    private Role role = Role.USER;

}
