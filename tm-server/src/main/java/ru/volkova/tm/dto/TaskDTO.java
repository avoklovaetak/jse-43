package ru.volkova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.entity.IWBS;
import ru.volkova.tm.model.Project;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "task")
public class TaskDTO extends AbstractOwnerEntityDTO implements IWBS {

    @Nullable
    @ManyToOne
    private Project project;

    @NotNull
    public String toString() {
        return id + ": " + getName() + ": " + project.getId()
                + "; " + "created: " + getCreated() +
                "started: " + getDateStart();
    }

}
