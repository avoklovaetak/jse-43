package ru.volkova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.repository.IRepositoryDTO;
import ru.volkova.tm.dto.AbstractEntityDTO;

import javax.persistence.EntityManager;

public abstract class AbstractDTORepository<E extends AbstractEntityDTO> implements IRepositoryDTO<E> {

    protected final EntityManager entityManager;

    protected AbstractDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

}
