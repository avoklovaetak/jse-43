package ru.volkova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.dto.ITaskDTORepository;
import ru.volkova.tm.dto.TaskDTO;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.entity.TaskNotFoundException;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskDTORepository extends AbstractDTORepository<TaskDTO> implements ITaskDTORepository {

    public TaskDTORepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @Nullable
    public TaskDTO insert(@Nullable TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        entityManager.persist(task);
        return task;
    }

    @Override
    public void clear(@NotNull String userId) {
        entityManager
                .createQuery("DELETE FROM task t WHERE t.user_id = :userId")
                .setParameter("userId", userId);
    }

    @Override
    @NotNull
    public List<TaskDTO> findAll(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT t FROM task t", TaskDTO.class)
                .getResultList();
    }

    @Override
    public void bindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    ) {
        entityManager
                .createQuery("UPDATE task t SET t.project_id = :projectId" +
                                "WHERE t.user_id = :userId AND t.id = :id ",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    @NotNull
    public List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId) {
        return entityManager
                .createQuery("SELECT t FROM task t " +
                        "WHERE t.project_id = :projectId", TaskDTO.class)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(@NotNull String userId, @NotNull String projectId) {
         entityManager
                .createQuery("DELETE t FROM task t " +
                        "WHERE t.project_id = :projectId", TaskDTO.class)
                .setParameter("projectId", projectId);
    }

    @Override
    public void unbindTaskByProjectId(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        entityManager
                .createQuery("UPDATE task t SET t.project_id = :projectId" +
                                "WHERE t.user_id = :userId AND t.id = :id ",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .setParameter("projectId", null)
                .executeUpdate();
    }

    @Override
    @Nullable
    public TaskDTO findById(@NotNull String userId, @NotNull String id) {
        return entityManager
                .createQuery("SELECT t FROM task t " +
                                "WHERE t.user_id = :userId AND t.id = :id",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public TaskDTO findOneByIndex(@NotNull String userId, @NotNull Integer index) {
        return entityManager
                .createQuery("SELECT t FROM task t WHERE t.user_id = :userId",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public TaskDTO findOneByName(@NotNull String userId, @NotNull String name) {
        return entityManager
                .createQuery("SELECT t FROM task t " +
                                "WHERE t.user_id = :userId AND t.name = :name",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public void changeOneStatusById(
            @NotNull String userId,
            @Nullable String id,
            @Nullable Status status
    ) {
        entityManager
                .createQuery("UPDATE task t SET t.status = :status" +
                                "WHERE t.user_id = :userId AND t.id = :id",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setParameter("status", status)
                .executeUpdate();
    }

    @Override
    public void changeOneStatusByName(
            @NotNull String userId,
            @Nullable String name,
            @Nullable Status status
    ) {
        entityManager
                .createQuery("UPDATE task t SET t.status = :status" +
                                "WHERE t.user_id = :userId AND t.name = :name",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setParameter("status", status)
                .executeUpdate();
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        entityManager
                .createQuery("DELETE FROM task t " +
                        "WHERE t.user_id = :userId and t.id = :id", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByName(@NotNull String userId, @NotNull String name) {
        entityManager
                .createQuery("DELETE FROM task t " +
                        "WHERE t.user_id = :userId and t.name = :name", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void updateOneById(
            @NotNull String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) {
        entityManager
                .createQuery("UPDATE task t SET t.name = :name, t.description = :description" +
                                "WHERE t.user_id = :userId AND t.id = :id",
                        TaskDTO.class)
                .setParameter("name", name)
                .setParameter("description", description)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

}
