package ru.volkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.repository.model.ISessionRepository;
import ru.volkova.tm.model.Session;

import javax.persistence.EntityManager;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull Session session) {
        entityManager.persist(session);
    }

    @Override
    public void close(@NotNull Session session) {
        entityManager.createQuery("DELETE t FROM session t", Session.class);
    }

}
