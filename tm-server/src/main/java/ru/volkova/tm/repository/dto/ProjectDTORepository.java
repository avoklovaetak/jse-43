package ru.volkova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.dto.IProjectDTORepository;
import ru.volkova.tm.dto.ProjectDTO;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectDTORepository extends AbstractDTORepository<ProjectDTO> implements IProjectDTORepository {

    public ProjectDTORepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @Nullable
    public ProjectDTO insert(@Nullable ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        entityManager.persist(project);
        return project;
    }

    @Override
    public void clear(@NotNull String userId) {
        entityManager
                .createQuery("DELETE FROM project t WHERE t.user_id = :userId")
                .setParameter("userId", userId);
    }

    @Override
    @NotNull
    public List<ProjectDTO> findAll(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT t FROM project t", ProjectDTO.class)
                .getResultList();
    }

    @Override
    @Nullable
    public ProjectDTO findById(@NotNull String userId, @NotNull String id) {
        return entityManager
                .createQuery("SELECT t FROM project t WHERE t.user_id = :userId AND t.id = :id",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public ProjectDTO findOneByIndex(@NotNull String userId, @NotNull Integer index) {
        return entityManager
                .createQuery("SELECT t FROM project t WHERE t.user_id = :userId",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public ProjectDTO findOneByName(@NotNull String userId, @NotNull String name) {
        return entityManager
                .createQuery("SELECT t FROM project t WHERE t.user_id = :userId AND t.name = :name",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public void changeOneStatusById(@NotNull String userId, @Nullable String id, @Nullable Status status) {
        entityManager
                .createQuery("UPDATE project t SET t.status = :status" +
                                "WHERE t.user_id = :userId AND t.id = :id",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setParameter("status", status)
                .executeUpdate();
    }

    @Override
    public void changeOneStatusByName(@NotNull String userId, @Nullable String name, @Nullable Status status) {
        entityManager
                .createQuery("UPDATE project t SET t.status = :status" +
                                "WHERE t.user_id = :userId AND t.name = :name ",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setParameter("status", status)
                .executeUpdate();
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        entityManager
                .createQuery("DELETE FROM project t WHERE t.user_id = :userId and t.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByName(@NotNull String userId, @Nullable String name) {
        entityManager
                .createQuery("DELETE FROM project t WHERE t.user_id = :userId and t.name = :name")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void updateOneById(
            @NotNull String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) {
        entityManager
                .createQuery("UPDATE project t SET t.name = :name, t.description = :description" +
                                "WHERE t.user_id = :userId AND t.id = :id",
                        ProjectDTO.class)
                .setParameter("name", name)
                .setParameter("description", description)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

}
