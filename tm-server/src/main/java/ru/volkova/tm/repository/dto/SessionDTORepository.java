package ru.volkova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.repository.dto.ISessionDTORepository;
import ru.volkova.tm.dto.SessionDTO;
import ru.volkova.tm.model.Session;

import javax.persistence.EntityManager;

public class SessionDTORepository extends AbstractDTORepository<SessionDTO> implements ISessionDTORepository {

    public SessionDTORepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull SessionDTO session) {
        entityManager.persist(session);
    }

    @Override
    public void close(@NotNull SessionDTO session) {
        entityManager.createQuery("DELETE t FROM session t", Session.class);
    }

}
