package ru.volkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.repository.IRepository;
import ru.volkova.tm.model.AbstractEntity;

import javax.persistence.EntityManager;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final EntityManager entityManager;

    protected AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

}
