package ru.volkova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

public class UserByEmailLockCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "lock user by email";
    }

    @Override
    public void execute() {
        if (bootstrap == null) throw new ObjectNotFoundException();
        if (endpointLocator == null) throw new ObjectNotFoundException();
        @Nullable final Session session = bootstrap.getSession();
        System.out.println("[LOCK USER]");
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        endpointLocator.getAdminUserEndpoint().lockByEmail(session, email);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String name() {
        return "user-lock-by-email";
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
