package ru.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.endpoint.Project;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import java.util.List;

public class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "show project list";
    }

    @Override
    public void execute() {
        if (bootstrap == null) throw new ObjectNotFoundException();
        if (endpointLocator == null) throw new ObjectNotFoundException();
        System.out.println("[PROJECT LIST]");
        @Nullable final Session session = bootstrap.getSession();
        List<Project> projects = endpointLocator.getProjectEndpoint().findAllProjects(session);
        int index = 1;
        for (final Project project : projects) {
            System.out.println((index + ". " + project));
            index++;
        }
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
